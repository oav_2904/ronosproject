# Servidor de Correo en DEBIAN

Lo primero que se debe realizar para la elaboración de la maquina virtual en el servidor de Azure se debe de buscar en el panel de recursos el sistema operativo que deseemos usar en ste caso un sistema GNU/Linux como lo es Debian.

## Creación de la máquina virtual

Para este proyecto en específico decidimos usar una imagen de Debian 10 "Buster" pra asi lograr 
un buen nivel de desempeño de la máquina con varios recursos como lo son

    Disco Duro = SSD estándar de 128Gb
    RAM = 3.5 Gb

Además de esos recursos utilizamos los estándares:

    SSH
    HTTP
    HTTPS

Para así aprovechar la mayor cantidad de puertos de entrada y salida al servidor de correos electronicos.

## Configuración del subdominio

Para crear el subdominio del WebMail , se utiliza la ip pública que brida la maquina desde el
server de Azure y comunicarla al adminsrador del dominio principal para que configure el subdominio
y así poder empezar la configuracion del servicio

## Configuración de servidor local
Primero se debe de estar en modo administrador o super usuario para poder realizar estas configuraciones

Se procede a descargar el archivo postfix para iniciar con el trabajocon el comando:

    apt-get install postfix
    
Luego se procede a editar el archivo main.cf dentro de postfix:

    nano /etc/postfix/main.cf 

Se procede a cambiar los valores del hostname, origin y destination respectivamente y la red en 
la que esta localizado nuestro servidor de correos en este caso
la ip pública que nos brinda Azure se procede a guardar los cambios y salimos del archivo

Luego se debe reiniciar el servicio postfix

    service postfix restart
Luego se debe de agregar las utilidades de correo mediante

    apt-get install mailutils
    
Luego se crean los usuarios que se deseen mediante diferentes comandos:

    adduser (nombre a utilizar)
    
Luego le pedira una contraseña para ingresar a ese usuario y tambien sus datos personales para 
configurar totalmente su cuenta

Para realizar pruebas se debe logear al usuario mediante el comando:

    login
    (nombre de usuario)
    (contraseña)
    mail (usuario receptor del correo)
    
Y para revisar si sirvio de manera correcta se ingresa al usuario receptor de la misma manera y 
se revisa si tiene correo mediante el comando:

    mail
    
Si todo fue capturado de manera correcta deberia mostrarle el correo que recibio.

## Configuracion de WebMail RoundCube

Podemos instalar Postfix, Dovecot, MariaDB como alternativa de MySQL, rkhunter y Binutils con un solo comando:

    apt-get -y install postfix postfix-mysql postfix-doc
    mariadb-client mariadb-server openssl getmail4 rkhun
    ter binutils dovecot-imapd dovecot-pop3d dovecot-mys
    ql dovecot-sieve dovecot-lmtpd sudo

Se le harán las siguientes preguntas:

Nombre de correo del sistema de sitio de Internet : rosca.isw612.xyz

Para asegurar la instalación de MariaDB y deshabilitar la base de datos de prueba, ejecute este comando:

    mysql_secure_installation

Responda las preguntas de la siguiente manera:

    Change the root password? [Y/n] <-- y    
    New password: <-- Nueva contraseña
    Re-enter new password: <-- repetir contraseña
    Remove anonymous users? [Y/n] <-- y
    Disallow root login remotely? [Y/n] <-- y
    Remove test database and access to it? [Y/n] <-- y
    Reload privilege tables now? [Y/n] <-- y
    
A continuación, abra los puertos TLS / SSL y de envío en Postfix:

    nano /etc/postfix/master.cf

Descomentar la presentación y SMTPS secciones de la
siguiente manera y añadir líneas en las que tan necesario que
esta sección de la apariencia de archivos master.cf exactamente
como la de abajo. IMPORTANTE: ¡ Elimine el # delante de las líneas
que comienzan con smtps y envío también y no solo de las
líneas -o después de estas líneas!

Reinicie Postfix luego:

    systemctl restart postfix

Queremos que MySQL escuche en todas las interfaces, no solo
localhost. Por lo tanto, editamos
/etc/mysql/mariadb.conf.d/50-server.cnf y
comentamos la línea bind-address = 127.0.0.1
agregando un # delante de ella.

    nano /etc/mysql/mariadb.conf.d/50-server.cnf
    
Establezca el método de autenticación de contraseña en MariaDB
como nativo para que podamos usar PHPMyAdmin más tarde
para conectarse como usuario root:

    echo "update mysql.user set plugin = 'mysql_native_password' where user='root';" | mysql -u root

Edite el archivo /etc/mysql/debian.cnf y configure la contraseña 
raíz MYSQL / MariaDB allí dos veces en las filas que comienzan 
con la palabra contraseña.

    nano /etc/mysql/debian.cnf
    
Para evitar el error ' Error al aceptar: demasiados archivos abiertos ', 
estableceremos límites de archivos abiertos más altos para MariaDB ahora.

    nano /etc/security/limits.conf

y se agregan estas líneas al final del archivo.

    mysql soft nofile 65535
    mysql hard nofile 65535
    
A continuación, cree un nuevo directorio

    mkdir -p /etc/systemd/system/mysql.service.d/
    
y agregue un nuevo archivo dentro:

    nano /etc/systemd/system/mysql.service.d/limits.conf
    
agregue las siguientes líneas en ese archivo:

    [Service]
    LimitNOFILE=infinity
    
Guarde el archivo y cierre el nano editor.
Luego recargamos systemd y reiniciamos MariaDB:

    systemctl daemon-reload
    systemctl restart mariadb
    
Ahora verifique que la red esté habilitada.

    netstat -tap | grep mysql

La salida debería verse así:

    root@server1:/home/administrator# netstat -tap | grep mysql
    tcp6 0 0 [::]:mysql [::]:* LISTEN 16623/mysqld
    
### Instalar el servidor web Apache y PHP

Apache2, PHP, FCGI, suExec, Pear y mcrypt se pueden instalar de la siguiente manera:

    apt-get -y install apache2 apache2-doc apache2-utils
    libapache2-mod-php php7.3 php7.3-common php7.3-gd ph
    p7.3-mysql php7.3-imap php7.3-cli php7.3-cgi libapac
    he2-mod-fcgid apache2-suexec-pristine php-pear mcryp
    t  imagemagick libruby libapache2-mod-python php7.3-
    curl php7.3-intl php7.3-pspell php7.3-recode php7.3-
    sqlite3 php7.3-tidy php7.3-xmlrpc php7.3-xsl memcach
    ed php-memcache php-imagick php-gettext php7.3-zip p
    hp7.3-mbstring memcached libapache2-mod-passenger ph
    p7.3-soap php7.3-fpm php7.3-opcache php-apcu
    
Luego, ejecute el siguiente comando para habilitar los módulos
de Apache suexec , rewrite , ssl , actions e include 
(más dav , dav_fs y auth_digest si desea usar WebDAV):

    a2enmod suexec rewrite ssl actions include dav_fs da
    v auth_digest cgi headers actions proxy_fcgi alias
    
Para garantizar que el servidor no pueda ser atacado a través de
la vulnerabilidad HTTPOXY , deshabilitaremos el encabezado
HTTP_PROXY en apache globalmente agregando el archivo de configuración

    nano /etc/apache2/conf-available/httpoxy.conf
    
Agregue el siguiente contenido en el archivo:

    <IfModule mod_headers.c>
        RequestHeader desarmó Proxy temprano
    </IfModule>
    
Y habilite el módulo ejecutando:

    a2enconf httpoxy
    systemctl restart apache2
    
### Instalar Mailman

ISPConfig le permite administrar (crear / modificar / eliminar)
listas de correo de Mailman. Si desea utilizar esta función, instale
Mailman de la siguiente manera:

    apt-get install mailman

Seleccione al menos un idioma, por ejemplo:

    Languages to support: <-- en (English)
    Missing site list <-- Ok

Antes de que podamos iniciar Mailman, se debe crear una
primera lista de correo llamada mailman :

    root@server1:~# newlist mailman
    Enter the email of the person running the list: <-- admin email address, e.g. rosca@isw612.xyz
    Initial mailman password: <-- contraseña de admin
    To finish creating your mailing list, you must edit your /etc/aliases (or
    equivalent) file by adding the following lines, and possibly running the
    `newaliases' program:
    ## mailman mailing list
    mailman:              "|/var/lib/mailman/mail/mailman post mailman"
    mailman-admin:        "|/var/lib/mailman/mail/mailman admin mailman"
    mailman-bounces:      "|/var/lib/mailman/mail/mailman bounces mailman"
    mailman-confirm:      "|/var/lib/mailman/mail/mailman confirm mailman"
    mailman-join:         "|/var/lib/mailman/mail/mailman join mailman"
    mailman-leave:        "|/var/lib/mailman/mail/mailman leave mailman"
    mailman-owner:        "|/var/lib/mailman/mail/mailman owner mailman"
    mailman-request:      "|/var/lib/mailman/mail/mailman request mailman"
    mailman-subscribe:    "|/var/lib/mailman/mail/mailman subscribe mailman"
    mailman-unsubscribe:  "|/var/lib/mailman/mail/mailman unsubscribe mailman"
    Hit enter to notify mailman owner... <-- ENTER
    
Abra / etc / alias después

    nano /etc/aliases
    
y agregue las siguientes líneas:

    [...]
    ## lista de correo cartero
    cartero: "| / var / lib / mailman / mail / mailman post mailman"
    mailman-admin: "| / var / lib / mailman / mail / mailman admin mailman"
    mailman-bounces: "| / var / lib / mailman / mail / mailman devuelve mailman"
    mailman-confirm: "| / var / lib / mailman / mail / mailman confirm mailman"
    mailman-join: "| / var / lib / mailman / mail / mailman join mailman"
    mailman-leave: "| / var / lib / mailman / mail / mailman leave mailman"
    mailman-owner: "| / var / lib / mailman / mail / mailman owner mailman"
    mailman-request: "| / var / lib / mailman / mail / mailman request mailman"
    mailman-subscribe: "| / var / lib / mailman / mail / mailman subscribe mailman"
    mailman-unsubscribe: "| / var / lib / mailman / mail / mailman cancelar suscripción mailman"
    
Para correr ese sistema:

    newaliases
    
Y se reinicia el Postfix:

    systemctl restart postfix

Finalmente, debemos habilitar la configuración de Mailman Apache:

    ln -s /etc/mailman/apache.conf /etc/apache2/conf-enabled/mailman.conf
    
Esto define el alias / cgi-bin / mailman / para todos los
vhosts de Apache, lo que significa que puede acceder a la 
interfaz de administración de Mailman para obtener una lista en
http://server1.example.com/cgi-bin/mailman/admin/ , y La página
web para usuarios de una lista de correo se puede encontrar en
http://rosca.isw612.xyz/cgi-bin/mailman/listinfo/ .

En http://rosca.isw612.xyz/pipermail puede encontrar los archivos de la lista de correo.

Reinicie Apache luego:

    systemctl restart apache2

Luego Reinicia el Mailman:

    systemctl restart mailman
    
### Instalar RoundCube Webmail

Instalaremos el cliente de correo web RoundCube.
Primero, tenemos que crear la base de datos para Roundcube manualmente ya que actualmente
hay un problema en el instalador de RoundCube Debian que hace que no pueda crear la base de datos automáticamente.
Ejecute este comando para crear la base de datos:

    echo "CREATE DATABASE roundcube;" | mysql --defaults-file=/etc/mysql/debian.cnf
    
Luego instale RoundCube con este comando:

    apt-get install roundcube roundcube-core roundcube-mysql roundcube-plugins
    
El instalador hará las siguientes preguntas:

    Configure database for roundcube with dbconfig.common? <-- yes
    MySQL application password for roundcube: <-- press enter
    
Luego edite el archivo RoundCube /etc/roundcube/config.inc.php y ajuste algunas configuraciones:

    nano /etc/roundcube/config.inc.php
    
Establezca default_host y smtp_server en localhost.

    $config['default_host'] = 'localhost';
    $config['smtp_server'] = 'localhost';
    
Luego edite el archivo de configuración de RoundCube de Apache:

    nano /etc/apache2/conf-enabled/roundcube.conf
    
Y agregue una línea de alias para el alias de apache / webmail y una para / roundcube, 
puede agregar la línea justo al comienzo del archivo. 
NOTA: ¡No use / mail como alias o el módulo de correo electrónico ispconfig dejará de funcionar!

    Alias /roundcube /var/lib/roundcube
    Alias /webmail /var/lib/roundcube
    
Luego vuelva a cargar Apache:

    systemctl reload apache2
    
Ahora puede acceder a RoundCube de la siguiente manera:

    http://rosca.isw612.xyz:8080/webmail

